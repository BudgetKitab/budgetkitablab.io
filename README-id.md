[![Banner](images/banner.webp)](#overview)

Pilih bahasa: [Inggris](https://gitlab.com/BudgetKitab/budgetkitab.gitlab.io) | [Arab](https://gitlab.com/BudgetKitab/budgetkitab.gitlab.io/-/blob/main/README-ar.md)

## Overview
[BudgetKitab](https://budgetkitab.gitlab.io/) adalah Aplikasi Web multibahasa yang dirancang untuk membantu mahasiswa [Islamic University of Madinah](https://iu.edu.sa/) mengelola tunjangan buku _Badal Kutub_ mereka. _Badal Kutub_ adalah tunjangan buku tahunan sebesar 850 Riyal, biasanya dibagikan pada September, yang diberikan oleh universitas kepada mahasiswanya. Aplikasi ini menyediakan antarmuka yang intuitif untuk mencatat pembelian buku dan menampilkan anggaran yang tersisa.

## Fitur
- **Pencatatan Pembelian**: Masukkan nama buku, harga, dan waktu pembelian untuk melacak pengeluaran Anda.
- **Pelacakan Anggaran**: Aplikasi ini menghitung dan menampilkan anggaran _Badal Kutub_ Anda yang tersisa setelah setiap pembelian.
- **Penyimpanan Data**: Semua data disimpan di `localStorage`, memastikan informasi Anda tidak akan hilang dan menghilangkan kebutuhan untuk login.
- **Dukungan Multibahasa**: Aplikasi ini mendukung bahasa Arab, Inggris, dan Indonesia.
- **Ekspor Data**: Unduh entri data atau catatan pembelian Anda sebagai file XLSX untuk cadangan atau tujuan lainnya.
- **Perencanaan Anggaran**: Ramalkan anggaran Anda yang tersisa berdasarkan pembelian buku masa depan yang potensial.

## Instalasi
Dapat digunakan langsung di browser web atau diinstal di perangkat Anda untuk penggunaan offline. Untuk menginstal, cukup kunjungi [situs web BudgetKitab](https://budgetkitab.gitlab.io/) dan ikuti petunjuk untuk menambahkan aplikasi ke layar utama Anda.

## Penggunaan
1. Buka aplikasi dan pilih bahasa Anda.
2. Setiap kali Anda membeli buku, masukkan nama buku dan harga di bidang yang ditentukan.
3. Aplikasi akan secara otomatis menghitung dan menampilkan anggaran Anda yang tersisa.

## Kontribusi
Kontribusi dipersilakan! :)

## Lisensi
Proyek ini dilisensikan di bawah ketentuan [lisensi MIT](https://tlo.mit.edu/).
