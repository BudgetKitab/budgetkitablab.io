[![Banner](images/banner.webp)](#overview)
Choose a language: [Arabic](https://gitlab.com/BudgetKitab/budgetkitab.gitlab.io/-/blob/main/README-ar.md) | [Indonesian](https://gitlab.com/BudgetKitab/budgetkitab.gitlab.io/-/blob/main/README-id.md)

## Overview
[BudgetKitab](https://budgetkitab.gitlab.io/) is a multilingual Web App designed to help students at the [Islamic University of Madinah](https://iu.edu.sa/) manage their _Badal Kutub_ book allowance. _Badal Kutub_ is a yearly book allowance of 850 Riyals, provided by the university to its students. This app provides an intuitive interface for logging book purchases and displays the remaining budget.

## Features
- **Purchase Logging**: Enter the book name, price, and time of purchase to keep track of your spending.
- **Budget Tracking**: The app calculates and displays your remaining _Badal Kutub_ budget after each purchase.
- **Data Storage**: All data is stored in `localStorage`, ensuring your information won't be lost and eliminating the need for login.
- **Multilingual Support**: The app supports Arabic, English, and Indonesian languages.
- **Data Export**: Download your data entries or purchase records as an XLSX file for backup or other purposes.
- **Budget Planning**: Forecast your remaining budget based on potential future book purchases.

## Installation
It can be used directly in a web browser or installed on your device for offline use. To install, simply visit [the website](https://budgetkitab.gitlab.io/) and follow the prompts to add the app to your home screen.

## Usage
1. Open the app and select your language.
2. Each time you purchase a book, enter the book name and price in the designated fields.
3. The app will automatically calculate and display your remaining budget.

## Contributing
Contributions are welcome! :)

## License
This project is licensed under the terms of the [MIT license](https://tlo.mit.edu/).
