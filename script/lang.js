let langElement = document.getElementById('lang');

langElement.addEventListener('change', e => applyTranslation(e.target.value));

const applyTranslation = async (lang) => {
    try {
        localStorage.setItem('selectedLanguage', lang);
        console.log(`Language set to ${lang} and stored in localStorage with key 'selectedLanguage'.`);

        const response = await fetch(`/languages/${lang}.json`);
        const data = await response.json();

        for (let key in data) {
            let elements = document.querySelectorAll(`[i18n="${key}"]`);
            elements.forEach((element) => {
                element.textContent = data[key];
            });
        }

        document.body.setAttribute('dir', lang === 'ar' ? 'rtl' : 'ltr');
        document.body.style.cssText = lang === 'ar' ? 'font-family: Tajawal; font-size: 0.8rem;' : '';

    } catch (error) {
        console.error(`An error occurred while applying translation: ${error}`);
    }
}

window.addEventListener('load', () => {
    const storedLang = localStorage.getItem('selectedLanguage') || 'en';
    langElement.value = storedLang;
    applyTranslation(storedLang);
});

