const entryForm = document.getElementById("entryForm"),
    deleteAllButton = document.getElementById("deleteAllButton"),
    yearSelect = document.getElementById("year"),
    entriesWrapper = document.getElementById("entrieswrapper"),
    entriesTable = document.getElementById("entries"),
    totalSpentElement = document.getElementById("totalSpent"),
    progressBar = document.getElementById("progressBar"),
    BASE = 850;

const select = document.getElementById("year");
select.innerHTML = Array.from({ length: 11 }, (e, t) => {
    let r = t + 2018;
    return `<option value="${r}" ${2023 === r ? "selected" : ""}>${r}</option>`;
}).join("");

let yearlyEntries = JSON.parse(localStorage.getItem("yearlyEntries")) || {},
    currentYear = yearSelect.value,
    entries = yearlyEntries[currentYear] || [];

function checkEntriesAndDisplayWrapper() {
    entriesWrapper.style.display = yearlyEntries[currentYear]?.length > 0 ? "flex" : "none";
}

function updateLocalStorage() {
    localStorage.setItem("yearlyEntries", JSON.stringify(yearlyEntries));
    checkEntriesAndDisplayWrapper();
}

function calculateTotalSpent() {
    return entries.reduce((e, t) => e + t.price, 0);
}

function calculateBudget(totalSpent) {
    totalSpentElement.innerText = totalSpent;
    let remainingBudget = BASE - totalSpent;
    document.getElementById("budget").textContent = remainingBudget;
    return remainingBudget;
}


function displayEntries() {
    for (; entriesTable.rows.length > 1;) entriesTable.deleteRow(1);
    entries.forEach((e, t) => {
        let r = entriesTable.insertRow(-1);
        r.className = "row-animate";
        ["date", "book", "price"].forEach((t, n) => r.insertCell(n).textContent = e[t]);
        let n = document.createElement("button");
        n.innerHTML = '<span class="material-symbols-rounded">delete_forever</span>';
        n.addEventListener("click", () => deleteEntry(t));
        r.insertCell(3).appendChild(n);
    });
    let t = calculateTotalSpent();
    progressBar.style.width = (BASE - t) / BASE * 100 + "%";
    calculateBudget(t);
}

function deleteEntry(e) {
    entries.splice(e, 1);
    yearlyEntries[currentYear] = entries;
    updateLocalStorage();
    displayEntries();
}

yearSelect.value = currentYear;
checkEntriesAndDisplayWrapper();

yearSelect.addEventListener("change", () => {
    entries = yearlyEntries[currentYear = yearSelect.value] || [];
    displayEntries();
    checkEntriesAndDisplayWrapper();
});

entryForm.addEventListener("submit", e => {
    e.preventDefault();
    let { date: t, book: r, price: n } = entryForm;
    yearlyEntries[currentYear] = yearlyEntries[currentYear] || [];
    yearlyEntries[currentYear].push({
        date: t.value || new Date().toISOString().split("T")[0],
        book: r.value,
        price: Math.round(parseFloat(n.value))
    });
    entries = yearlyEntries[currentYear];
    updateLocalStorage();
    displayEntries();
    entryForm.reset();
});

deleteAllButton.addEventListener("click", () => {
    if (confirm("Are you sure?")) {
        entries.length = 0;
        yearlyEntries[currentYear] = entries;
        updateLocalStorage();
        displayEntries();
    }
});

const budgetBox = document.querySelector("#BudgetBox"),
    placeholder = document.querySelector("#placeholder"),
    sticky = budgetBox.offsetTop,
    boxHeight = `${budgetBox.offsetHeight}px`;

window.addEventListener("scroll", () => {
    let e = window.scrollY >= sticky;
    placeholder.style.height = e ? boxHeight : "";
    placeholder.style.display = e ? "flex" : "none";
    e ? budgetBox.classList.add("sticky") : budgetBox.classList.remove("sticky");
});

window.onload = displayEntries;
checkEntriesAndDisplayWrapper();

const updateBudgetColor = () => {
    const budgetElement = document.getElementById('budget');
    const progressBarElement = document.getElementById('progressBar');
    const budget = Number(budgetElement.textContent);
    const maxBudget = 850;
    const halfBudget = 425;

    if (budget > maxBudget) {
        progressBarElement.style.background = 'rgb(0, 83, 102)';
        return;
    }

    const colors = {
        green: [0, 83, 102],
        orange: [230, 150, 1],
        red: [114, 13, 13]
    };

    const [color1, color2] = budget >= halfBudget ? [colors.orange, colors.green] : [colors.red, colors.orange];
    const ratio = budget >= halfBudget ? (budget - halfBudget) / (maxBudget - halfBudget) : budget / halfBudget;

    const interpolatedColor = interpolateColor(color1, color2, ratio).join(', ');

    progressBarElement.style.background = `rgb(${interpolatedColor})`;
};