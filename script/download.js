const downloadButton = document.getElementById('downloadButton');
const downloadOptions = document.getElementById('downloadOptions');
const downloadXLSX = document.getElementById('downloadXLSX');
const backButton = document.getElementById('backButton');

downloadButton.addEventListener('click', () => toggleDisplay(downloadButton, downloadOptions));
backButton.addEventListener('click', () => toggleDisplay(downloadOptions, downloadButton));

const toggleDisplay = (hideElement, showElement) => {
    hideElement.style.display = 'none';
    showElement.style.display = 'flex';
}

const generateFilename = () => {
    const now = new Date();
    const formattedDate = `${now.getDate()}-${now.getMonth() + 1}-${now.getFullYear()}`;
    const formattedTime = `${now.getHours()}-${now.getMinutes()}`;
    return `BudgetKitab_${formattedDate}_${formattedTime}`;
}

downloadXLSX.addEventListener('click', () => {
    const filename = `${generateFilename()}.xlsx`;
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, XLSX.utils.json_to_sheet(
        Object.entries(yearlyEntries).flatMap(([year, entries]) =>
            entries.map(entry => ({ Year: year, Date: entry.date, Book: entry.book, Price: entry.price }))
        )
    ), "Sheet1");
    XLSX.writeFile(wb, filename);
});