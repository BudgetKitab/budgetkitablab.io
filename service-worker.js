importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.4.1/workbox-sw.js');

if (workbox) {
  console.log(`Workbox is loaded`);
  workbox.googleAnalytics.initialize();
} else {
  console.log(`Workbox didn't load 😬`);
}

const cacheName = 'app-shell-' + 'UNIQUE_ID';

self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open(cacheName).then(function (cache) {
      return cache.addAll([
        '/',
        '/index.html',
        '/styles/dark.css',
        '/script/index.js',
        '/script/lang.js',
        '/script/download.js',
        '/languages/ar.json',
        '/languages/en.json',
        '/languages/id.json',
        '/images/icons-192.png',
        '/images/icons-512.png',
        'https://unpkg.com/xlsx/dist/xlsx.full.min.js',
        'https://fonts.cdnfonts.com/css/product-sans',
        'https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200',
        'https://fonts.googleapis.com/css2?family=Almarai:wght@700&display=swap'
      ]).catch(function (error) {
        console.error('Failed to cache:', error);
      });
    })
  );
  self.skipWaiting();
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        return response || fetch(event.request);
      })
      .catch(function (error) {
        console.error('Failed to fetch:', event.request.url);
        throw error;
      })
  );
});

self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(
        cacheNames.filter(function (cacheName) {
          return cacheName.startsWith('app-shell-') && cacheName !== 'app-shell-UNIQUE_ID';
        }).map(function (cacheName) {
          return caches.delete(cacheName);
        })
      );
    })
  );
  self.clients.claim();
});
